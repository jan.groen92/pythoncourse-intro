"""
Exercise warmup1
-----------------

Simple exercises to get used to python basics.

- From http://introtopython.org/var_string_num.html , do the following:
  - hello world
  - one variable, two messages
  - first name cases
  - full name
  - arithmetic

"""

# Write your solution here

#Hello World excercise

a = 'Hello Carlos.'
print(a)

#One Variable, Two Messages

a = 'Thanks for giving this course!'
print(a)

#First Name Cases

b = 'jan'
b.upper()
b.lower()
b.capitalize()

#Full Name

c = 'groen'
print(b.capitalize(), c.capitalize())

#arithmetic

d = 10
e = 5

print(d+e)
print(d-e)
print(d*e)
print(d/e)
print(d**e)